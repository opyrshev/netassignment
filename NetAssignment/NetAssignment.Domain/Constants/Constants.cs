﻿namespace NetAssignment.Domain.Constants
{
    public static class Constants
    {
        public const decimal Tax = 0.07M;

        public const decimal Margin = 0.11M;

        public const decimal ExtraMargin = 0.05M;
    }
}
