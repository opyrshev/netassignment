﻿namespace NetAssignment.Domain.Models
{
    public class JobElement
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool? IsExempt { get; set; }

    }
}
