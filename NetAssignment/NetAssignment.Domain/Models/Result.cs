﻿using System.Collections.Generic;

namespace NetAssignment.Domain.Models
{
    public class Result
    {
        public decimal Total { get; set; }

        public List<ResultElement> Items { get; set; } = new List<ResultElement>();
    }
}
