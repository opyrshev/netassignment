﻿namespace NetAssignment.Domain.Models
{
    public class ResultElement
    {
        public string Name { get; set; }

        public decimal Amount { get; set; }
    }
}
