﻿using System.Collections.Generic;

namespace NetAssignment.Domain.Models
{
    public class Job
    {
        public bool? IsExtraMargin { get; set; }

        public List<JobElement> Items { get; set; } = new List<JobElement>();
    }
}
