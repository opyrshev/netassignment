﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetAssignment.Application.Interfaces
{
    public interface ICalculationService
    {
        Task<string> GetResult(string inputData);
    };
}
