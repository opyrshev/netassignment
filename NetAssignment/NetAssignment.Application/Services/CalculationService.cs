﻿using NetAssignment.Application.Interfaces;
using NetAssignment.Domain.Models;
using NetAssignment.Domain.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetAssignment.Application.Services
{
    public class CalculationService : ICalculationService
    {
        public async Task<string> GetResult(string inputData)
        {
            if(string.IsNullOrEmpty(inputData))
            {
                return await Task.FromResult(string.Empty);
            }

            var job = JsonConvert.DeserializeObject<Job>(inputData);
            var res = JsonConvert.SerializeObject(ResultToDictionary(ResultCalculation(job)));

            return await Task.FromResult(res);
        }

        private Result ResultCalculation(Job job)
        {
            var result = new Result();
            result.Total = 0M;

            foreach (var jobItem in job.Items)
            {
                var resultElement = new ResultElement();

                resultElement.Name = jobItem.Name;

                var tax = TaxCalculation(jobItem.IsExempt) * jobItem.Price;
                var margin = MarginCalculation(job.IsExtraMargin) * jobItem.Price;
                
                resultElement.Amount = Math.Truncate((jobItem.Price + tax) * 100 ) / 100M;

                result.Total += Math.Truncate((jobItem.Price + tax + margin) * 100 ) / 100M;

                result.Items.Add(resultElement);
            }

            return result;
        }

        private Dictionary<string, string> ResultToDictionary(Result result)
        {
            var items = new Dictionary<string, string>();
            if (result != null)
            {
                foreach (var element in result.Items)
                {
                    items.Add(element.Name, string.Format("${0:0.00}", element.Amount));
                }
                items.Add("total", string.Format("${0:0.00}", result.Total));
            }

            return items;
        }

        private decimal MarginCalculation(bool? isExtra)
        {
            return Constants.Margin + ((isExtra != null && (bool)isExtra) ? Constants.ExtraMargin : 0M);
        }

        private decimal TaxCalculation(bool? isExempt)
        {
            return (isExempt != null && (bool)isExempt) ? 0M : Constants.Tax;
        }
    }
}
