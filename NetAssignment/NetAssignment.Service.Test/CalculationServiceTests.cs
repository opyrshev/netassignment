﻿using NetAssignment.Application.Interfaces;
using NetAssignment.Application.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.Json;

namespace NetAssignment.Service.Test
{
    public class CalculationServiceTests
    {
        private ICalculationService _sut;
        private string[] jobs;
        private string[] expectedResults;
        private string[] jobPath =
        {
            "NetAssignment.Service.Test.Jobs.Job1.json",
            "NetAssignment.Service.Test.Jobs.Job2.json",
            "NetAssignment.Service.Test.Jobs.Job3.json"
        };
        private string[] resultPath =
        {
            "NetAssignment.Service.Test.Results.Result1.json",
            "NetAssignment.Service.Test.Results.Result2.json",
            "NetAssignment.Service.Test.Results.Result3.json"
        };

        [SetUp]
        public void Setup()
        {
            DataPrepare();
            _sut = new CalculationService();
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        public void Job_Test(int i)
        {
            var res = JsonSerializer.Deserialize<Dictionary<string, string>>(_sut.GetResult(jobs[i]).Result);
            var expRes = JsonSerializer.Deserialize<Dictionary<string, string>>(expectedResults[i]);
            Assert.AreEqual(res, expRes);
        }

        private string LoadJson(string embeddedPath)
        {
            using var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(embeddedPath);
            using var reader = new StreamReader(stream, Encoding.UTF8);
            return reader.ReadToEnd();
        }

        private void DataPrepare()
        {
            List<string> jobList = new();
            List<string> resultList = new();
            for (int i = 0; i < jobPath.Length; i++)
            {
                jobList.Add(LoadJson(jobPath[i]));
                resultList.Add(LoadJson(resultPath[i]));
            }

            jobs = jobList.ToArray();
            expectedResults = resultList.ToArray();
        }
    }
}
