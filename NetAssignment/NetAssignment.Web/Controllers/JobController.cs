﻿using Microsoft.AspNetCore.Mvc;
using NetAssignment.Application.Interfaces;
using System.Threading.Tasks;

namespace NetAssignment.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class JobController : ControllerBase
    {
        private readonly ICalculationService _service;
        public JobController(ICalculationService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetJobResult(string inputData)
        {
            var result = await _service.GetResult(inputData);

            return Ok(result);
        }
    }
}
